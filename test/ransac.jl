points = [1:100 1:100]'

@test (FindPlanes.ransac(points); true)
normal, distance = FindPlanes.ransac(points)
@test approxeq(distance, 0, atol = 1e-6)
@test approxeq(normal[1], -normal[2])

points = [1:100 -(1:100)]'

@test (FindPlanes.ransac(points); true)
normal, distance = FindPlanes.ransac(points)
@test approxeq(distance, 0, atol = 1e-6)
@test approxeq(normal[1], normal[2])

points = [1:100 (5 .- (1:100))]'

@test (FindPlanes.ransac(points); true)
normal, distance = FindPlanes.ransac(points)
@test approxeq(distance, sqrt(25 / 2))
@test approxeq(normal[1], normal[2])

points = [rand(100) rand(100) ones(100)]'

@test (FindPlanes.ransac(points); true)
normal, distance = FindPlanes.ransac(points)
@test approxeq(distance, 1)
@test approxeq(normal, [0, 0, 1])

points = [rand(110) rand(110) [ones(100); 10 .* ones(10)]]'

@test (FindPlanes.ransac(points); true)
normal, distance = FindPlanes.ransac(points)
@test approxeq(distance, 1)
@test approxeq(normal, [0, 0, 1])
