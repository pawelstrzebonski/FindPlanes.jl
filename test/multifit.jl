#TODO: Test with multiple lines and noise
#TODO: Test higher dimensions

points = [[1:100; 1:100; 3:6] [1:100; -(1:100); 2 .* (4:7)]]'

@test (FindPlanes.multiplanefit(points); true)
planes = FindPlanes.multiplanefit(points)
normals = getindex.(planes, 1)
distances = getindex.(planes, 2)
@test length(planes) == 3
@test isapprox(
    normals,
    [[1 / sqrt(2), 1 / sqrt(2)], [-1 / sqrt(2), 1 / sqrt(2)], [-2 / sqrt(5), 1 / sqrt(5)]],
) || isapprox(
    normals,
    [[-1 / sqrt(2), 1 / sqrt(2)], [1 / sqrt(2), 1 / sqrt(2)], [-2 / sqrt(5), 1 / sqrt(5)]],
)
@test isapprox(distances, [0, 0, sqrt(0.8)])
