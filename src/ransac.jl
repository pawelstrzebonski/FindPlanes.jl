import LinearAlgebra
import Random

# Mentions some methods:
# https://stackoverflow.com/questions/10900141/fast-plane-fitting-to-many-points

"""
    linearleastsquares(points::AbstractMatrix)->(normal, distance)

Linear least squares fit a set of points as a plane in normal-distance
form.
"""
function linearleastsquares(points::AbstractMatrix)
    X0, Y = points[1:end-1, :], points[end, :]
    X = hcat(X0', ones(size(X0, 2)))
    beta = inv(X' * X) * X' * Y
    normal = [beta[1:end-1]; -1]
    distance = -beta[end] / LinearAlgebra.norm(normal)
    -LinearAlgebra.normalize(normal), -distance
end

"""
    planefit(points::AbstractMatrix)->(normal, distance)

Use SVD to obtain a best fit plane.
"""
function planefit(points::AbstractMatrix)
    centroid = sum(points, dims = 2) / size(points, 2)
    centeredpoints = points .- centroid
    U, S, V = LinearAlgebra.svd(centeredpoints)
    normal = U[:, end]
    distance = sum(points' * normal) / size(points, 2)
    normal, distance
end

"""
    ransac(points::AbstractMatrix; maxiter::Integer = 50, maxdist::Number = 0.1, minfracpoints::Number = 0.1, numpoints = nothing, fitmet = :SVD)->(normal, distance)

Use RANSAC to obtain a best-fit hyperplane for a subset of the points in
normal-distance form.

# Arguments
- `points::AbstractMatrix`: array of the points (where `points[:, i]` is a single point).
- `maxiter::Integer = 50`: number of RANSAC iterations.
- `maxdist::Number = 0.1`: maximal distance from a plane for a point to be considered an element of it.
- `minfracpoints::Number = 0.1`: minimal fraction of all of the points within `maxdist` of a plane to be considered a good fit.
- `numpoints = nothing`: number of random points used to define test planes (defaults to the number of dimensions of the space).
- `fitmet = :SVD`: method used to fit a set of points to a plane (`:SVD` or `:LLSF`)

RANSAC is an iterative algorithm that will randomly sample `numpoints`
elements from `points`, fit a plane to those points, and count the number
of all `points` that fall within `maxdist` of said plane. The best result
(the one which fits the most points) will be used to generate a new
plane, fitted to all of the points that fell within the distance limit.

The algorithm fitting the points to a plane is specified by `fitmet`.
`:LLSF` is the linear least squares fit, which is efficient but may fail
for some sets of points. `:SVD` uses an SVD-based algorithm that avoids
some of those issues but may be slower. 
"""
function ransac(
    points::AbstractMatrix;
    maxiter::Integer = 50,
    maxdist::Number = 0.1,
    minfracpoints::Number = 0.1,
    numpoints = nothing,
    fitmet = :SVD,
)
    Ndim = size(points, 1)
    Npoints = size(points, 2)
    numpoints = isnothing(numpoints) ? Ndim : numpoints

    fit = if fitmet == :SVD
        planefit
    elseif fitmet == :LLSF
        linearleastsquares
    else
        @error "Bad `fitmet` given (should be either `:SVD` or `LLSF`)."
    end

    best_normal = ones(Ndim)
    best_distance = 0.0
    best_points = 0
    for it = 1:maxiter
        # Obtain a random subset of points
        randpts = Random.randperm(Npoints)[1:Ndim]
        # Fit a plane to those points
        normal, distance = fit(points[:, randpts])
        # Find all points within a threshold distance of that plane
        distances = abs.(points' * normal .- distance)
        inliers = distances .<= maxdist
        num_points = sum(inliers)
        # If this plane fits more points than the previous best, then save it
        if num_points >= minfracpoints * Npoints && num_points > best_points
            @info string("Found a plane with ", num_points, " out of ", Npoints)
            # Obtain a better fit by fitting all of the found inliers
            normal, distance = fit(points[:, inliers])
            best_points = num_points
            best_normal = normal
            best_distance = distance
        end
        # If all of the points have been fitted, then we may just break the loop
        if num_points == Npoints
            break
        end
    end
    # Throw an error if no plane has been found
    if iszero(best_points)
        error("No plane has been found to satisfy the `minfracpoints` condition.")
    end
    @info string("Finished: Fitted ", best_points, " points out of ", Npoints)
    best_normal, best_distance
end
