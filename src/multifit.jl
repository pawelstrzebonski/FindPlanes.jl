"""
    multiplanefit(points::AbstractMatrix; maxplanes::Integer = 10, maxiter::Integer = 50, maxdist::Number = 0.1, minfracpoints::Number = 0.1, numpoints = nothing, fitmet = :SVD)

Iteratively find multiple planes in `points` by finding individual
planes using RANSAC, removing the fitted points, and repeating on the
remaining points. The identified planes are returned as an array of
`(normal, distance)` tuples, each describing a plane in normal-distance
form.

See `ransac` for more explanation concerning the method and options.

# Arguments
- `points::AbstractMatrix`: array of the points (where `points[:, i]` is a single point).
- `maxplanes::Integer = 10`: maximal number of planes to search for.
- `maxiter::Integer = 50`: number of RANSAC iterations.
- `maxdist::Number = 0.1`: maximal distance from a plane for a point to be considered an element of it.
- `minfracpoints::Number = 0.1`: minimal fraction of all of the points within `maxdist` of a plane to be considered a good fit.
- `numpoints = nothing`: number of random points used to define test planes (defaults to the number of dimensions of the space).
- `fitmet = :SVD`: method used to fit a set of points to a plane (`:SVD` or `:LLSF`)
"""
function multiplanefit(
    points0::AbstractMatrix;
    maxplanes::Integer = 10,
    maxiter::Integer = 50,
    maxdist::Number = 0.1,
    minfracpoints::Number = 0.1,
    numpoints = nothing,
    fitmet = :SVD,
)
    points = deepcopy(points0)
    planes = []
    for i = 1:maxplanes
        try
            # Find a plane
            normal, distance = ransac(
                points,
                maxiter = maxiter,
                maxdist = maxdist,
                minfracpoints = minfracpoints,
                numpoints = numpoints,
                fitmet = fitmet,
            )
            # Remove all the points within that plane
            distances = abs.(points' * normal .- distance)
            points = points[:, distances.>maxdist]
            # Return the plane information
            append!(planes, [(normal, distance)])
        catch
            # If we don't find a plane, then we break the loop
            break
        end
    end
    if size(points, 2) > 0
        @info string(
            "After plane fitting we have ",
            size(points, 2),
            " points left over out of ",
            size(points0, 2),
        )
    end
    planes
end
