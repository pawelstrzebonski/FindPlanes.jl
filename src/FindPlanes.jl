module FindPlanes

include("ransac.jl")
include("multifit.jl")

export ransac, multiplanefit

end
