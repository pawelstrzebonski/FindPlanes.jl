# FindPlanes.jl Documentation

`FindPlanes` is a package for finding lines and planes in sets of points.

## Installation

This package is not in the official Julia package repository, so to
install, run in Julia:

```Julia
]add https://gitlab.com/pawelstrzebonski/FindPlanes.jl
```

## Features

* Implement [RANSAC](https://en.wikipedia.org/wiki/Ransac) for finding a plane in arbitrary dimensions N>1
* Implement basic multi-plane identification built on RANSAC
