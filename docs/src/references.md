# References

The basic plane-finding algorithm used is RANSAC, which can be found
described on its
[Wikipedia page](https://en.wikipedia.org/wiki/Ransac).
Plane-fitting is implemented using either linear least squares
([Wikipedia page](https://en.wikipedia.org/wiki/Linear_least_squares_\(mathematics\)))
or an SVD based method described in a
[StackOverflow post](https://stackoverflow.com/a/10904220)
which may or may not refer to total least squares
([Wikipedia page](https://en.wikipedia.org/wiki/Total_least_squares)).
