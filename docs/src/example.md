# Example Usage

## `ransac`

The basic hyperplane finding functionality is provided by the `ransac`
function that will find a single hyperplane (due to the nature of the
algorithm, the results are not necessarily deterministic). As an example,
lets create a set of points on a line and see what `ransac` finds:

```@example ex
import FindPlanes
points=[1:100 5 .+ 2 .* (1:100)]'
normal, distance=FindPlanes.ransac(points)
```

This package returns results in the form of a normal vector and a
distance, aka
[Hesse normal form](https://en.wikipedia.org/wiki/Hesse_normal_form).
As all of the given points are on the line we find that they
satisfy the equation for a plane in Hesse normal form ($n\cdot r - d=0$,
with some some allowance for floating point error):

```@example ex
all(abs.(points'*normal .- distance) .< 1e-9)
```

We can similarly provide a set of points in 3D (or any higher dimension)
and obtain the normal vector and distance for a hyperplane:

```@example ex
points=[2 .* ones(100) rand(100) rand(100)]'
normal, distance=FindPlanes.ransac(points)
```

```@example ex
all(abs.(points'*normal .- distance) .< 1e-9)
```

Of course, RANSAC is intended to find planes in noisy data, so we add
an additional set of "noise" points:

```@example ex
points=[[2 .* ones(100);rand(100)] rand(200) rand(200)]'
normal, distance=FindPlanes.ransac(points)
```

Assuming that the function finds the desired plane (it may fail due to
the random nature of RANSAC), we should find that the resulting plane
fits the first 100 points well:

```@example ex
all(abs.(points'*normal .- distance)[1:100] .< 1e-9)
```

But the random points, of course, do not, with many quite far from
the found plane:

```@example ex
sum(abs.(points'*normal .- distance)[101:200] .> 0.1)
```

If `ransac` fails to find the plane we're looking for, we could increase
the number of iterations by increasing the value of the `maxiter` option.

## `multiplanefit`

If we have a set of points representing multiple hyperplanes, then we
can use the `multiplanefit` function to find them. `multiplanefit`
uses `ransac` to iteratively find a hyperplane in our dataset and
filter out the points belonging to that hyperplane before searching
for the next plane. We try with our simple single line dataset:

```@example ex
import FindPlanes
points=[1:100 5 .+ 2 .* (1:100)]'
planes=FindPlanes.multiplanefit(points)
```

`multiplanefit` returns an array of tuples, each containing the normal
vector and distance defining a plane. We repeat with a dataset with
two lines:

```@example ex
points=[[1:100; 1:100] [5 .+ 2 .* (1:100); 1:100]]'
planes=FindPlanes.multiplanefit(points)
```

We add noise points:

```@example ex
points=[[1:100; 1:100; 100*rand(100)] [5 .+ 2 .* (1:100); 1:100; 100*rand(100)]]'
planes=FindPlanes.multiplanefit(points)
```

We should find the original 2 lines, and due to the random nature of the
noise and the algorithm we may or may not find other lines. Hyperplanes
will only be returned if they fit a sufficient fraction of the points
(defined by `minfracpoints`). If we had a large number of planes,
only up to `maxplanes` could be found.

As with `ransac`, `multiplanefit` supports higher dimensions as
well. Refer to function documentation
for a list and explanation of the various options.
