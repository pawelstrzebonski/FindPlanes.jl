# Contributing and Development

Contributions to this package are welcomed. Please submit issues/pull-requests
on the GitLab project.

## TODOs

The following are some areas for development and improvement:

* More, better unit tests and documentation
* Code improvement (performance and resilience)
* Better algorithms (especially for finding multiple planes in a dataset)
* Support for non-planar surfaces (allow for quadratic surface fitting?)

## Guidance

When adding functionality or making modifications, please keep in mind
the following:

* We aim to support finding hyperplanes in multiple dimensions (at least in 2D and 3D, but ideally in arbitrary N-D)
* We aim to minimize the amount of code and function repetition when implementing the above features
