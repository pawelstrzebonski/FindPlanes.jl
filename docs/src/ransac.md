# ransac.jl

Implement
[RANSAC](https://en.wikipedia.org/wiki/Ransac)
for finding hyperplanes in arbitrary dimensions N>1.

```@autodocs
Modules = [FindPlanes]
Pages   = ["ransac.jl"]
```
