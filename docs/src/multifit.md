# multifit.jl

Find multiple planes in a dataset by repeatedly running RANSAC on a set
of points that has progressively had it's planes filtered out.

```@autodocs
Modules = [FindPlanes]
Pages   = ["multifit.jl"]
```
