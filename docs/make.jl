using Documenter
import FindPlanes

makedocs(
    sitename = "FindPlanes.jl",
    repo = Documenter.Remotes.GitLab("pawelstrzebonski", "FindPlanes.jl"),
    pages = [
        "Home" => "index.md",
        "Example Usage" => "example.md",
        "References" => "references.md",
        "Contributing" => "contributing.md",
        "src/" => ["multifit.jl" => "multifit.md", "ransac.jl" => "ransac.md"],
        "test/" => ["multifit.jl" => "multifit_test.md", "ransac.jl" => "ransac_test.md"],
    ],
)
