# FindPlanes

## About

`FindPlanes.jl` is a package for finding lines and planes in sets of points.

## Installation

This package is not in the official Julia package repository, so to
install, run in Julia:

```Julia
]add https://gitlab.com/pawelstrzebonski/FindPlanes.jl
```

## Documentation

Package documentation and usage examples can be found at the
[Gitlab Pages for FindPlanes.jl](https://pawelstrzebonski.gitlab.io/FindPlanes.jl/).
